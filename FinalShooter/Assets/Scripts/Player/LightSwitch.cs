﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class LightSwitch : MonoBehaviour {

	Animator anim; 
	public bool isYellow;
    public GameObject enemyManager;
	public GameObject speedText;
	public GameObject tankText;
    public AudioClip tankSound;
    public AudioClip speedSound;
    

    // Use this for initialization
    void Start () 
	{
        //List<EnemyManager> enemyManagerList = new List<EnemyManager>(enemyManager.GetComponents<EnemyManager>());
        anim = GetComponent<Animator>();
    } 
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Space))
        {
            List<EnemyManager> enemyManagerList = new List<EnemyManager>(enemyManager.GetComponents<EnemyManager>());
            //print(enemyManagerList[0].spawnTime);
            isYellow = !isYellow;
			anim.SetBool ("Switch", !anim.GetBool("Switch"));
            
            if (isYellow)
			{
                enemyManagerList[0].SetSpawnSpeed(1.75f);
                enemyManagerList[1].SetSpawnSpeed(1.75f);
                enemyManagerList[2].SetSpawnSpeed(5.0f);

				speedText.transform.DOMoveX (-200, .5f);
				tankText.transform.DOMoveX (10, 1);

                AudioSource audio = GetComponent<AudioSource>();
                audio.PlayOneShot(tankSound,2f);
            }
            else
            {
                enemyManagerList[0].SetSpawnSpeed(3.0f);
                enemyManagerList[1].SetSpawnSpeed(3.0f);
                enemyManagerList[2].SetSpawnSpeed(7.0f);

				tankText.transform.DOMoveX (-200, 1);
				speedText.transform.DOMoveX (10, 0.5f);

                AudioSource audio = GetComponent<AudioSource>();
                audio.PlayOneShot(speedSound, 2f);
            }
            
            //print(enemyManager.GetComponent<EnemyManager>().spawnTime);
		}
	}
}