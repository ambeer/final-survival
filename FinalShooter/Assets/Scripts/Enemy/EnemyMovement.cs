﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
    Transform player;
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    UnityEngine.AI.NavMeshAgent nav;


    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag ("Player").transform;
        playerHealth = player.GetComponent <PlayerHealth> ();
        enemyHealth = GetComponent <EnemyHealth> ();
        nav = GetComponent <UnityEngine.AI.NavMeshAgent> ();
        //print(nav.speed);
    }


    void Update ()
    {
        if (!GameObject.Find("Light").GetComponent<LightSwitch>().isYellow)
        {
           nav.speed = 2.5f;
        }

        if (GameObject.Find("Light").GetComponent<LightSwitch>().isYellow)
        {
           nav.speed = 3.25f;
        }





        if (enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0)
        {
            nav.SetDestination (player.position);
        }
        else
        {
            nav.enabled = false;
        }
    }
}
