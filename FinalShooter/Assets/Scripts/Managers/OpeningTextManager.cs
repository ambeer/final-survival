﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class OpeningTextManager : MonoBehaviour {

    public GameObject MovementText;
    float timer;

	// Use this for initialization
	void Start ()
    {
        ShowText();
	}
	
	// Update is called once per frame
	void Update ()
    {
        timer += Time.deltaTime;

        if (Input.anyKey && timer > 6)
        {
            RemoveText();
        } 

        /*if (timer > 1)
        {
            ShowText();
        }
        if (Input.anyKey)
        {
            RemoveText();
        }*/
    }
    
    void ShowText()
    {
        MovementText.transform.DOMoveY(400, 1);
    }

    void RemoveText()
    {
        MovementText.transform.DOMoveX(-700, 1);
    }
}
